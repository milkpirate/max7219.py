#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: Paul Schroeder
"""

import mock_littleWire
import pytest

import py7219


@pytest.mark.parametrize("lat_pin, lw_pin", [
    pytest.param(0, mock_littleWire.PIN4),
    pytest.param(1, mock_littleWire.PIN1),
    pytest.param(2, mock_littleWire.PIN2),
    pytest.param(3, mock_littleWire.PIN3),
    pytest.param(4, mock_littleWire.PIN4),
    pytest.param(5, mock_littleWire.PIN3),
])
def test_constructor_lat_pin(max_instance, lat_pin, lw_pin):
    max_ic = max_instance(lat_pin=lat_pin)
    print("=0", max_ic, max_ic.lw)

    pins = [mock_littleWire.LOW] * 6
    pins[lw_pin] = mock_littleWire.HIGH

    assert max_ic.lw.pins == pins, "Lat pin does not match"
    assert max_ic.lw.mode == [mock_littleWire.LOW] * 6, "mode registers not 0"


def test_constructor_common_anode(max_instance, boolean_range):
    max_ic = max_instance(common_anode=boolean_range)
    assert max_ic.common_anode == boolean_range, "wrong comman anode"


def test_constructor_number_of_digits(max_instance, digit_range):
    max_ic = max_instance(number_of_digits=digit_range)
    assert max_ic.number_of_digits == digit_range, "wrong number of digits"


def test_clear(max_instance):
    max_ic = max_instance()

    max_ic.frame_buffer = [0x55] * 8
    max_ic.clear()
    assert max_ic.lw.spi["send_buffer"] == [max_ic.op_code.digit7, 0x00], "last spi transmit incorrect"
    assert max_ic.frame_buffer == [0] * 8, "frame buffer not 0"


def test_spi_init(max_instance):
    max_ic = max_instance()
    assert max_ic.lw.spi["send_buffer"] == [max_ic.op_code.digit7, 0x00], "last spi transmit incorrect"
    assert max_ic.lw.spi["init"], "spi init did not happen"


@pytest.mark.parametrize("opcode, byte", [
    pytest.param(py7219.Max.op_code.nop,            0x34),
    pytest.param(py7219.Max.op_code.digit5,         0x98),
    pytest.param(py7219.Max.op_code.decode_mode,    0x23),
    pytest.param(py7219.Max.op_code.intensity,      0xab),
    pytest.param(py7219.Max.op_code.scan_limit,     0xf3),
    pytest.param(py7219.Max.op_code.shutdown,       0x5c),
    pytest.param(py7219.Max.op_code.display_test,   0x6d),
    pytest.param(13,                                0xff),
    pytest.param(14,                                0x01),
])
def test_spi_transfer_opcode_exceptions(max_instance, opcode, byte):
    max_ic = max_instance()

    # preconditions
    pins = [mock_littleWire.LOW] * 6
    pins[max_ic.lat_pin] = mock_littleWire.HIGH
    max_ic.lw.pins[max_ic.lat_pin] = mock_littleWire.LOW
    max_ic.lw.spi["mode"] = mock_littleWire.AUTO_CS

    # function under test
    if opcode not in vars(py7219.Max.op_code).values():
        with pytest.raises(ValueError):
            max_ic.spi_transfer(opcode, byte)
    else:
        max_ic.spi_transfer(opcode, byte)
        assert max_ic.lw.pins[max_ic.lat_pin] == mock_littleWire.HIGH, "lat pin not high"
        assert max_ic.lw.spi["mode"] == mock_littleWire.MANUAL_CS, "spi mode not set to MANUAL_CS"
        assert max_ic.lw.spi["send_buffer"] == [opcode, byte], "send buffer does not match"

