codeclimate-test-reporter==0.2.3
coverage==4.0.3
pytest==3.5.0
pytest-cov==2.5.1
pytest-xdist==1.22.2
python-coveralls==2.9.1
