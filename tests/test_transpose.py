#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: Paul Schroeder
"""

import pytest

import py7219


@pytest.mark.parametrize("inp, outp, desired", [
    pytest.param([255, 0, 0, 0, 0, 0, 0, 0], [128] * 8, True),
    pytest.param([0, 0, 0, 0, 0, 0, 0, 255], [1] * 8, True),
    pytest.param([236, 37, 10, 189, 152, 250, 158, 9], [158, 132, 212, 30, 191, 210, 38, 81], True),
])
def test_transpose_in_out(inp, outp, desired):
    assert (py7219.Max.transpose(inp) == outp) == desired, "result does not match desired"

