#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: Paul Schroeder
"""

import collections


# General Purpose Pins
PIN1 = 1
PIN2 = 2
PIN3 = 5
PIN4 = 0


# Useful constants
INPUT = 1
OUTPUT = 0

HIGH = 1
LOW = 0

AUTO_CS = 1
MANUAL_CS = 0


class device(object):
    """
    Mock littleWire class
    """

    spi_max_history = 32

    def __init__(self):
        self.pins = [0]*6
        self.mode = [0]*6
        self.spi = dict(
            init=False,
            history=collections.deque(maxlen=self.spi_max_history)
        )

    def pinMode(self, pin, mode):
        self.mode[pin] = 1 if mode else 0

    def digitalWrite(self, pin, state):
        self.pins[pin] = 1 if state else 0

    def spi_init(self):
        self.spi["init"] = True

    def spi_sendMessageMulti(self, send_buffer, length, mode):
        self.spi.update(locals())
        self.spi["history"].append(send_buffer)
