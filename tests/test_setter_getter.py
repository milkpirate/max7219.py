#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: Paul Schroeder
"""


import pytest


@pytest.mark.parametrize("in_str, preset, exp_frame_buffer", [
    pytest.param("Abccccccccccccc", [0] * 8, [26, 26, 26, 26, 26, 26, 62, 238]),
    pytest.param("Abcc", [0x55] * 8, [0x55, 0x55, 0x55, 0x55, 26, 26, 62, 238]),
    pytest.param("Abcc  ", [0x55] * 8, [0x55, 0x55, 0, 0, 26, 26, 62, 238]),
    pytest.param("", [0x55] * 8, [0]*8),
])
def test_write_str(max_instance, in_str, preset, exp_frame_buffer):
    # regular tests
    max_ic = max_instance(number_of_digits=8)
    max_ic.frame_buffer = preset
    max_ic.write_string(in_str)
    assert max_ic.frame_buffer == exp_frame_buffer, "frame buffer does not match expected one"
    assert max_ic.lw.spi["send_buffer"] == [max_ic.op_code.digit7, exp_frame_buffer[-1]], "last spi transmit was not correct"

    # skipping test with common_anode=True, because the only difference is the py7219.transpose method (which is
    # thoroughly tested) before transmitting the whole frame_buffer transposed.


def test_set_brightness(max_instance, brightness_range):
    max_ic = max_instance()
    max_ic.set_brightness(brightness_range)
    assert max_ic.lw.spi["send_buffer"] == [max_ic.op_code.intensity, brightness_range], "send buffer does not match expected one"


@pytest.mark.parametrize("preset, digit, in_chr, dp, exp_frame_buffer", [
    pytest.param([0] * 8, 3, "A", True, [0, 0, 239, 0, 0, 0, 0, 0]),
    pytest.param([0] * 8, 2, "A", False, [0, 238, 0, 0, 0, 0, 0, 0]),
    pytest.param([0x55] * 8, 5, "c", 1, [0x55, 0x55, 0x55, 0x55, 27, 0x55, 0x55, 0x55]),
    pytest.param([0x55] * 8, 5, "X", 0, [0x55, 0x55, 0x55, 0x55, 0, 0x55, 0x55, 0x55]),
])
def test_set_char(max_instance, preset, digit, in_chr, dp, exp_frame_buffer):
    max_ic = max_instance(number_of_digits=6)
    max_ic.frame_buffer = preset
    max_ic.set_char(digit, in_chr, dp)
    # assert max_ic.lw.spi["send_buffer"] == [digit, exp_frame_buffer[-1]]
    assert max_ic.frame_buffer == exp_frame_buffer, "frame buffer does not match expected one"


@pytest.mark.parametrize("preset, num_of_digit, anode, digit, in_chr, exp_frame_buffer, op_code_digit, buffer_val", [
    pytest.param([0]*8, 8, False, 3, 0x55, [0, 0, 0x55, 0, 0, 0, 0, 0], "digit2", 0x55),
    pytest.param([0]*8, 8, True,  8, 255,  [255, 0, 0, 0, 0, 0, 0, 0],  "digit7", 128),
    pytest.param([0]*8, 8, True,  1, 255,  [0, 255, 0, 0, 0, 0, 0, 0],  "digit7", 64),
])
def tests_set_digit(max_instance, preset, num_of_digit, anode, digit, in_chr, exp_frame_buffer, op_code_digit, buffer_val):
    max_ic = max_instance(number_of_digits=num_of_digit, common_anode=anode)
    max_ic.frame_buffer = preset
    max_ic.set_digit(digit, in_chr)
    assert max_ic.frame_buffer == exp_frame_buffer, "frame buffer does not match expected one"
    # in common_anode=False only changed digits are written
    # op codes are 0 based
    opcode_dict = vars(max_ic.op_code)  # convert namespace to dict
    assert max_ic.lw.spi["send_buffer"] == [opcode_dict[op_code_digit], buffer_val],  "send buffer does not match expected one"


def test_get_digit(max_instance, pin_range):
    max_ic = max_instance(number_of_digits=8)
    max_ic.frame_buffer = [1, 2, 3, 4, 5, 6, 7, 8]
    assert pin_range == max_ic.get_digit(pin_range), "gotten pin does not match"
