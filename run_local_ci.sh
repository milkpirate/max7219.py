#!/bin/sh

UNAME_OS="$(uname -s)"
case "${UNAME_OS}" in
    Linux*)     MACHINE="linux";;
    Darwin*)    MACHINE="darwin";;
    CYGWIN*)    MACHINE="windows";;
    MSYS*)      MACHINE="windows";;
    MINGW*)     MACHINE="windows";;
    *)
        echo "Unknown OS: ${UNAME_OS}"
        exit 1
    ;;
esac

UNAME_ARCH="$(uname -m)"
case "${UNAME_ARCH}" in
    x86)    ARCH="386";;
    i?86)   ARCH="386";;
    amd64)  ARCH="amd64";;
    x86_64) ARCH="amd64";;
    *)
        echo "Unknown arch: ${UNAME_ARCH}"
        exit 2
    ;;
esac

echo "Running $MACHINE on $ARCH"
[ "$MACHINE" = "windows" ] && EXT=".exe"

if [ ! -f ./gitlab-runner ]; then
    curl -o gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-${MACHINE}-${ARCH}${EXT}
    if [ $? -ne 0 ]; then
        rm gitlab-runner
        exit 3
    fi
fi

chmod +x gitlab-runner
./gitlab-runner exec docker "$@"
