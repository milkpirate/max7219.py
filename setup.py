#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Note: To use the 'upload' functionality of this file, you must:
#   $ pip install twine


import os
import pathlib
import shutil
import sys


from setuptools import find_packages, setup, Command


# Package meta-data.
NAME = 'py7219'
DESCRIPTION = 'Wrapper class for easy communication with the MAX7219 IC.'
URL = 'https://gitlab.com/milkpirate/py7219'
AUTHOR = 'Paul Schroeder'
# EMAIL = 'mail@mail.com'
REQUIRES_PYTHON = '>=3.6.0'
VERSION = '1.0'
LICENSE = 'GNU GPL v3'

# What packages are required for this module to be executed?
REQUIRED = [
    'pyusb==1.0.2',
]

# The rest you shouldn't have to touch too much :)
# ------------------------------------------------
# Except, perhaps the License and Trove Classifiers!
# If you do change the License, remember to change
# the Trove Classifier for that!

pwd = pathlib.PurePath(__file__).parent

# Import the README and use it as the long-description.
# Note: this will only work if 'README.md' is present in your MANIFEST.in file!
with open(pwd / 'README.md', encoding='utf-8') as file:
    long_description = '\n' + file.read()

# Load the package's __version__.py module as a dictionary.
about = {}
if not VERSION:
    with open(pwd / '__version__.py') as file:
        exec(file.read(), about)
else:
    about['__version__'] = VERSION


class UploadCommand(Command):
    """Support setup.py upload."""

    description = 'Build and publish the package.'
    user_options = []

    @staticmethod
    def status(string):
        """Prints things in bold."""
        print(f'\033[1m{string}\033[0m')

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            self.status('Removing previous builds…')
            shutil.rmtree(pwd / 'dist')
        except OSError:
            pass

        self.status('Building Source and Wheel (universal) distribution…')
        os.system(f'{sys.executable} setup.py sdist bdist_wheel --universal')

        self.status('Uploading the package to PyPi via Twine…')
        os.system('twine upload dist/*')

        self.status('Pushing git tags…')
        os.system(f'git tag v{about["__version__"]}')
        os.system('git push --tags')

        sys.exit()


# Where the magic happens:
# General info
setup_args = dict(
    name=NAME,
    version=about['__version__'],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    # author_email=EMAIL,
    url=URL,
    license=LICENSE,
)

# Requirements
setup_args.update(
    python_requires=REQUIRES_PYTHON,
    install_requires=REQUIRED,
)

# Packaging
setup_args.update(
    # packages=find_packages(where=".", exclude=('tests',)),
    # package_dir={'': 'src'},
    # If your package is a single module, use this instead of 'packages':
    py_modules=['py7219'],
    zip_safe=False,
    include_package_data=True,
)

# Technical data
setup_args.update(
    keywords=[
        'littlewire',
        'hardware',
        'interfacing',
        'spi',
        'attiny',
        'max7219',
        'max7221',
    ],
    platforms=[
        'Linux',
        'Windows',
        'OSX'
    ],
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: MacOS',
        'Operating System :: POSIX :: Linux',
        'Topic :: Education',
        'Topic :: Education :: Testing',
        'Topic :: Games/Entertainment',
        'Topic :: Home Automation',
        'Topic :: Multimedia :: Video :: Display',
        'Topic :: System :: Hardware :: Hardware Drivers',
        'Topic :: Utilities',
    ],
)

# Support:
# $ setup.py publish
setup_args.update(
    cmdclass={
        'upload': UploadCommand,
    },
)

if __name__ == '__main__':
    setup(**setup_args)
