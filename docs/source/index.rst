.. py7219 documentation master file, created by
   sphinx-quickstart on Mon Nov  5 00:06:17 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to py7219's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   py7219

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
