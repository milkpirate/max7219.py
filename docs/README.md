# Setup Sphinx to generat docs/manual

Just run

```bash
$ cd /path/to/project
$ mkdir docs
$ cd docs/
$ sphinx-quickstart
```

This begins the configuration process. The defaults are generally fine, but the only thing you need to do is enable the autodoc extension when asked.

Assuming all of your docstrings have been written, you need to create the stubs for your project in your docs directory (these need to be recreated if new modules are added):

```bash
$ cd docs/
$ sphinx-apidoc -o source/ ../
```

Edit `docs/conf.py` as desired. Add the following to enable Sphinx to find your source files:

```python
import os
import sys
sys.path.insert(0, os.path.abspath('../'))
```

You might also want to change the theme

```python
html_theme = 'sphinx_rtd_theme'
```

or add some extensions:

```python
extensions = ['sphinx.ext.autodoc',
    'sphinx.ext.coverage',
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode']

napoleon_google_docstring = False
napoleon_use_param = False
napoleon_use_ivar = True
```

Further readings: [Automatic Python documentation with Sphinx autodoc and ReadTheDocs](https://www.simonho.ca/programming/automatic-python-documentation/)
