#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: Paul Schroeder
"""

__version__ = '1.0'
__release__ = f'{__version__}.0rc'
