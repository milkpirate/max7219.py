# MAX7219/21 IC over [littleWire][little wire] controller module

![](./docs/max_7219.jpg)

[![pipeline status][pipeline_status]](https://gitlab.com/milkpirate/py7219.py/commits/master)
[![coverage report][code_coverage]](https://gitlab.com/milkpirate/py7219/commits/master)


[![made-with-python][python_badge]](https://www.python.org/) 
[![made-with-sphinx-doc][sphinx_badge]](https://www.sphinx-doc.org/)

[![Version][version_badge]]()
[![GitHub license][license_badge]](https://gitlab.com/milkpirate/max7219.py/blob/master/LICENSE)
[![Open Source Love][open_source_badge]](https://github.com/ellerbrock/open-source-badges/)

This module's purpose is to simplify the handling of a [MAX7219/21][max7219 datesheet] IC.

It uses the VeryLittleWire [littleWire.py][very little wire] to communicate with the IC
(need to be downloaded/installed seperately)

[little wire]: http://littlewire.github.io/

[pipeline_status]: https://gitlab.com/milkpirate/py7219/badges/master/pipeline.svg "pipeline_status"
[code_coverage]: https://gitlab.com/milkpirate/py7219/badges/master/coverage.svg "code coverage"
[python_badge]: https://img.shields.io/badge/made%20with-Python-1f425f.svg "python badge"
[sphinx_badge]: https://img.shields.io/badge/made%20with-Sphinx-1f425f.svg "sphinx badge"
[version_badge]: https://img.shields.io/badge/version-0.9.0-blue.svg "version bagde"
[license_badge]: https://img.shields.io/badge/license-GPL%20v3-blue.svg "version bagde"
[open_source_badge]: https://badges.frapsoft.com/os/v2/open-source.svg?v=103 "version bagde"

[max7219 datesheet]: https://www.sparkfun.com/datasheets/Components/General/COM-09622-MAX7219-MAX7221.pdf
[very little wire]: https://github.com/adajoh99/VeryLittleWire
